package com.song.jiusonggao.scarnesdice;

import org.junit.Test;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void hashSet_example() {
        Set<Integer> set = new HashSet<>();
        set.add(1);
        set.add(2);
        set.add(3);

        for (Iterator i = set.iterator(); i.hasNext();) {
            Integer integer = (Integer) i.next();
            System.out.println(integer);
        }

        set.remove(2);

        for (Iterator i = set.iterator(); i.hasNext();) {
            Integer integer = (Integer) i.next();
            System.out.println(integer);
        }

    }
}