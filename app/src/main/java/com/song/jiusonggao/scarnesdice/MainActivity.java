package com.song.jiusonggao.scarnesdice;

import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.databinding.DataBindingUtil;
import android.view.View;

import com.song.jiusonggao.scarnesdice.databinding.ActivityMainBinding;

import java.util.Random;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private final static int MAX_COMPUTER_TURN_SCORE = 20;
    private final static int GAME_END_SCORE = 100;
    private final static int COMPUTER_ROLL_DICE_TIME = 500; //ms
    enum GameStatus {
        USER_TURN,
        COMPUTER_TURN,
        USER_WON,
        COMPTUER_WON
    }
    private static final int MAX_DICE_VALUE = 6;
    private int userOverallScore;
    private int computerOverallScore;
    private int turnScore;
    private ActivityMainBinding binding;
    private Random random = new Random();
    private GameStatus gameStatus;
    private RollDiceHandler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (binding == null) {
            binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        }
        initView();
        gameStatus = GameStatus.USER_TURN;
        updateGameBoard();
        handler = new RollDiceHandler();
    }

    private void initView() {
        binding.rollButton.setOnClickListener(this);
        binding.holdButton.setOnClickListener(this);
        binding.resetButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == binding.resetButton.getId()) {
            resetGame();
            updateGameBoard();
        } else if (view.getId() == binding.rollButton.getId()) {
            int diceValue = rollDice();
            updateDiceImage(diceValue);
            updateGameStatus(diceValue);
            updateGameBoard();
            if (gameStatus == GameStatus.COMPUTER_TURN) {
                setButtonsEnabled(false);
                computerRun();
            }
        } else if (view.getId() == binding.holdButton.getId()) {
            userOverallScore += turnScore;
            turnScore = 0;
            if (userOverallScore >= GAME_END_SCORE) {
                gameStatus = GameStatus.USER_WON;
            } else {
                gameStatus = GameStatus.COMPUTER_TURN;
                setButtonsEnabled(false);
                computerRun();
            }
            updateGameBoard();
        }
    }

    private void computerRun() {
        int diceValue = rollDice();
        handler.sendEmptyMessageDelayed(diceValue, COMPUTER_ROLL_DICE_TIME);
    }

    private int rollDice() {
        return random.nextInt(MAX_DICE_VALUE) + 1;
    }

    private void updateDiceImage(int diceValue) {
        binding.diceImageView.setImageResource(R.drawable.dice1 + diceValue -1);
    }

    private void resetGame() {
        userOverallScore = 0;
        computerOverallScore = 0;
        turnScore = 0;
        gameStatus = GameStatus.USER_TURN;
    }

    private void updateGameStatus(int diceValue) {
        if (diceValue == 1) {
            // Switch turn.
            turnScore = 0;
            if (gameStatus == GameStatus.USER_TURN) {
                gameStatus = GameStatus.COMPUTER_TURN;
            } else {
                gameStatus = GameStatus.USER_TURN;
            }
        } else {
            turnScore += diceValue;
            if (gameStatus == GameStatus.COMPUTER_TURN && turnScore > MAX_COMPUTER_TURN_SCORE) {
                computerOverallScore += turnScore;
                turnScore = 0;
                if (computerOverallScore > GAME_END_SCORE) {
                    gameStatus = GameStatus.COMPTUER_WON;
                } else {
                    gameStatus = GameStatus.USER_TURN;
                }
            }
        }
    }

    private void updateGameBoard() {
        // Update game score.
        String computerScore = getResources().getString(R.string.computer_score, computerOverallScore);
        String userScore = getResources().getString(R.string.your_score, userOverallScore);
        String turnScoreStr = getResources().getString(R.string.turn_score, turnScore);
        binding.gameScore.setText(userScore + " " + computerScore);
        binding.turnScore.setText(turnScoreStr);
        // Update game status.
        switch (gameStatus) {
            case COMPTUER_WON:
                binding.gameStatus.setText("Computer Won!");
                break;
            case USER_TURN:
                binding.gameStatus.setText("Your Turn");
                break;
            case COMPUTER_TURN:
                binding.gameStatus.setText("Computer Turn");
                break;
            case USER_WON:
                binding.gameStatus.setText("You won!");
                break;
            default:
                binding.gameStatus.setText("Your Turn");
                break;
        }
    }

    private void setButtonsEnabled(boolean enabled) {
        binding.holdButton.setEnabled(enabled);
        binding.rollButton.setEnabled(enabled);
        binding.resetButton.setEnabled(enabled);
    }

    /*
     * A class handles computer rolling dice.
     */
    class RollDiceHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            updateDiceImage(message.what);
            updateGameStatus(message.what);
            updateGameBoard();
            if (gameStatus == GameStatus.COMPUTER_TURN) {
                computerRun();
            } else {
                setButtonsEnabled(true);
            }
        }
    }
}
